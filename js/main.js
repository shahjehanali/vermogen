$(document).ready(function(){
    $('div[data-type="background"]').each(function(){
        var $bgobj = $(this); // assigning the object
    
        $(window).scroll(function() {
            var yPos = -($(window).scrollTop() / $bgobj.data('speed')); 
            
            // Put together our final background position
            var coords = '50% '+ yPos + 'px';

            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        }); 
    });    
    $("#sportswear").owlCarousel();
            $("#fitness-wear").owlCarousel();
            $("#casual-wear").owlCarousel();
            $('.hamburger').on('click', function(){
  
            $(this).toggleClass('checked');
            $(".header-wrap").toggleClass("full-height");
            $("html, body").toggleClass("ov-hidden");
                });
});